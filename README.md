Curso docker
---

[![pipeline status](https://gitlab.com/polegar/curso-docker/badges/master/pipeline.svg)](https://gitlab.com/polegar/curso-docker/-/commits/master)

[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

Curso básico de utilização do docker

Autor
---

Luan <luan@ufpa.br>
