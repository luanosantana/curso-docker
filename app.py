


from flask import Flask
from redis import Redis
import socket
import os

app = Flask(__name__)

redis_server = os.environ.get('REDIS_SERVER', 'redis')
redis_port   = os.environ.get('REDIS_PORT', 6379)
port_app     = os.environ.get('PORT_APP', 5000)

# Comentario
redis = Redis(host=redis_server, port=redis_port);

@app.route('/')
def counter():
  redis.incr('hits')
  return ({"quantidade": str(redis.get('hits')), "hostname": str(socket.gethostname())})

if __name__=="__main__":
    app.run(host="0.0.0.0", port=port_app, debug=True)
